// Tank Game
//
// Written by James C. De Ricco
// in collaboration with the McGann-Mercy High School
// Project Lead the Way CS class of 2018

/*
  The MIT License

  Copyright © 2018 James C. De Ricco

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

'use strict';

Array.prototype.append = function (array) {
  var self = this;
  array.forEach(function (e) {
    self.push(e);
  });
}

Array.prototype.remove = function (object) {
  var i = this.indexOf(object);
  if (i >= 0) {
    this.splice(i, 1);
  }
}

Array.prototype.allPairs = function () {
  if (this.length < 1) {
    return [];
  } else {
    var rest = this.slice(1);
    return rest.map(e => [this[0], e]).concat(rest.allPairs());
  }
}

Array.prototype.x = function () {
  return this[0];
}

Array.prototype.y = function () {
  return this[1];
}

function combine(proc, a1, a2) {
  var a3 = [];

  var i;
  for (i = 0; i < Math.min(a1.length, a2.length); i++) {
    a3.push(proc(a1[i], a2[i]));
  }

  if (i < a1.length) {
    a3.append(a1.slice(i));
  } else if (i < a2.length) {
    a3.append(a2.slice(i));
  }

  return a3;
}

// Vector arithmetic
Array.prototype.vadd = function (vector) {
  return combine((a, b) => a + b, this, vector);
}

Array.prototype.vsub = function (vector) {
  return vector.vsmul(-1).vadd(this);
}

Array.prototype.vmul = function (vector) {
  return combine((a, b) => a * b, this, vector);
}

Array.prototype.vsmul = function (scalar) {
  return this.map(c => scalar * c);
}

Array.prototype.vmag = function () {
  return Math.sqrt(this.map(c => c * c).reduce((a, b) => a + b));
}

Array.prototype.vdistance = function (vector) {
   return vector.vsub(this).vmag();
}

Array.prototype.vequal = function (vector) {
  if (vector.length !== this.length) {
    return false;
  }

  for (var i = 0; i < vector.length; i++) {
    if (vector[i] !== this[i]) {
      return false;
    }
  }

  return true;
}

function Rect(topLeft, width, height) {
  this._topLeft = topLeft;
  this._width = width;
  this._height = height;
}

Rect.prototype.topLeft = function () {
  return this._topLeft;
}

Rect.prototype.centerX = function () {
  return this._topLeft.x() + this._width / 2;
}

Rect.prototype.left = function () {
  return this._topLeft.x();
}

Rect.prototype.right = function () {
  return this._topLeft.x() + this._width;
}

Rect.prototype.top = function () {
  return this._topLeft.y();
}

Rect.prototype.bottom = function () {
  return this._topLeft.y() + this._height;
}

Rect.prototype.rectAboveCentered = function (width, height) {
  return new Rect([this.centerX() - width / 2, this.top() - height], width, height);
}

Rect.prototype.translate = function (vector) {
  return new Rect(this._topLeft.vadd(vector), this._width, this._height);
}

Rect.prototype.inside = function (point) {
  var minX = this._topLeft.x();
  var minY = this._topLeft.y();
  var maxX = minX + this._width;
  var maxY = minY + this._height;

  function between(min, max, n) {
    return n >= min && n <= max;
  }

  return between(minX, maxX, point.x()) && between(minY, maxY, point.y());
}

function clamp(a, b, n) {
  var min = Math.min(a, b);
  var max = Math.max(a, b);
  return Math.max(min, Math.min(max, n));
}

Rect.prototype.clamp = function (point) {
  return [clamp(this.left(), this.right(), point.x()),
          clamp(this.bottom(), this.top(), point.y())];
}

Rect.prototype.intersects = function (g) {
  if (g instanceof Rect) {
    return g.left() < this.right()
        && g.right() > this.left()
        && g.top() < this.bottom()
        && g.bottom() > this.top();
  } else if (g instanceof Circle) {
    return this.inside(g.center()) || (this.clamp(g.center()).vdistance(g.center()) < g.radius());
  } else {
    return false;
  }
}

Rect.prototype.draw = function () {
  ctx.fillRect(this._topLeft.x(), this._topLeft.y(), this._width, this._height);
}

function Circle(center, radius) {
  this._center = center;
  this._radius = radius;
}

Circle.prototype.center = function () {
  return this._center;
}

Circle.prototype.radius = function () {
  return this._radius;
}

Circle.prototype.intersects = function (g) {
  if (g instanceof Circle) {
    return g.center().vdistance(this.center()) < (g.radius() + this.radius());
  } else {
    return false;
  }
}

Circle.prototype.translate = function (vector) {
  return new Circle(this._center.vadd(vector), this._radius);
}

Circle.prototype.draw = function () {
  ctx.beginPath();
  ctx.arc(this._center.x(), this._center.y(), this._radius, 0, 2 * Math.PI);
  ctx.fill();
}

function Wall(rect, style) {
  this._rect = rect;
  this._style = style;
}

Wall.prototype.geometry = function () {
  return this._rect;
}

Wall.prototype.draw = function () {
  ctx.fillStyle = this._style;
  this._rect.draw();
}

Wall.prototype.update = function (dt) {
}

Wall.prototype.onCollide = function (other) {
}

function Tank(rect, style, cannonAngle) {
  this._rect = rect;
  this._style = style;
  this._armor = 20;
  this._lastDisplacement = [0, 0];
  this._velocity = [0, 0];
  this._maxSpeed = 100;
  this._cannonAngle = cannonAngle;
  this._cannonAngularVelocity = 0; // radians per second
  this._cannonAngleChangeSpeed = Math.PI / 4; // radians per second
  this._cannonSpeed = 1000;
}

Tank.prototype.geometry = function () {
  return this._rect;
}

Tank.prototype.draw = function () {
  ctx.fillStyle = this._style;
  this._rect.draw();
}

Tank.prototype.update = function (dt) {
  var displacement = this._velocity.vsmul(dt);
  this._rect = this._rect.translate(displacement);
  this._cannonAngle += this._cannonAngularVelocity * dt;
  this._lastDisplacement = displacement;
}

Tank.prototype.onCollide = function (other) {
  if (other instanceof Wall) {
    var displacement;
    if (this._rect.left() > other.geometry().left()) {
      displacement = [other.geometry().right() - this._rect.left(), 0];
    } else {
      displacement = [-1 * (this._rect.right() - other.geometry().left()), 0];
    }

    this._rect = this._rect.translate(displacement);
  } else if (other instanceof Missile) {
    this._armor--;

    if (this._armor < 1) {
      scene.remove(this);
    }
  }
}

Tank.prototype.armor = function () {
  return this._armor;
}

Tank.prototype.startMovingRight = function () {
  this._velocity = [this._maxSpeed, 0];
}

Tank.prototype.startMovingLeft = function () {
  this._velocity = [-this._maxSpeed, 0];
}

Tank.prototype.stopMoving = function () {
  this._velocity = [0, 0];
}

Tank.prototype.startIncreasingFireAngle = function () {
  this._cannonAngularVelocity = this._cannonAngleChangeSpeed;
}

Tank.prototype.startDecreasingFireAngle = function () {
  this._cannonAngularVelocity = -this._cannonAngleChangeSpeed;
}

Tank.prototype.stopChangingFireAngle = function () {
  this._cannonAngularVelocity = 0;
}

Tank.prototype.fireMissile = function () {
  if (this._armor > 0) {
    scene.push(new Missile(new Circle([this._rect.centerX(), this._rect.top() - 10], 8),
                                       this._velocity.vadd(makeVelocity(this._cannonAngle, this._cannonSpeed)),
                           this._style));
  }
}

function makeVelocity(angle, speed) {
  return [Math.cos(angle), -Math.sin(angle)].vsmul(speed);
}

function AbstractMoveController(startMoveLeft, startMoveRight, stopMoving) {
  this._startMoveLeft = startMoveLeft;
  this._startMoveRight = startMoveRight;
  this._stopMoving = stopMoving;

  this._leftDown = false;
  this._rightDown = false;
}

AbstractMoveController.prototype.onLeftDown = function () {
  this._leftDown = true;
  this._startMoveLeft();
}

AbstractMoveController.prototype.onLeftUp = function () {
  this._leftDown = false;
  if (this._rightDown) {
    this._startMoveRight();
  } else {
    this._stopMoving();
  }
}

AbstractMoveController.prototype.onRightDown = function () {
  this._rightDown = true;
  this._startMoveRight();
}

AbstractMoveController.prototype.onRightUp = function () {
  this._rightDown = false;
  if (this._leftDown) {
    this._startMoveLeft();
  } else {
    this._stopMoving();
  }
}

function TankController(tank) {
  this._tank = tank;
  this._moveController = new AbstractMoveController(() => tank.startMovingLeft(),
                                                    () => tank.startMovingRight(),
                                                    () => tank.stopMoving());
  this._cannonController = new AbstractMoveController(() => tank.startDecreasingFireAngle(),
                                                      () => tank.startIncreasingFireAngle(),
                                                      () => tank.stopChangingFireAngle());
}

TankController.prototype.onLeftDown = function () {
  this._moveController.onLeftDown();
}

TankController.prototype.onLeftUp = function () {
  this._moveController.onLeftUp();
}

TankController.prototype.onRightDown = function () {
  this._moveController.onRightDown();
}

TankController.prototype.onRightUp = function () {
  this._moveController.onRightUp();
}

TankController.prototype.onUpDown = function () {
  this._cannonController.onRightDown();
}

TankController.prototype.onUpUp = function () {
  this._cannonController.onRightUp();
}

TankController.prototype.onDownDown = function () {
  this._cannonController.onLeftDown();
}

TankController.prototype.onDownUp = function () {
  this._cannonController.onLeftUp();
}

function Missile(circle, velocity, style) {
  this._circle = circle;
  this._velocity = velocity;
  this._style = style;
}

Missile.prototype.geometry = function () {
  return this._circle;
}

Missile.prototype.draw = function () {
  ctx.fillStyle = this._style;
  this._circle.draw();
}

Missile.prototype.update = function (dt) {
  if (!sceneRect.inside(this._circle.center())) {
    scene.remove(this);
  } else {
    this._velocity = this._velocity.vadd(gravity.vsmul(dt));
    this._circle = this._circle.translate(this._velocity.vsmul(dt));
  }
}

Missile.prototype.onCollide = function (other) {
  scene.remove(this);
}

function ScoreBoard(tank1, tank2) {
  this._tank1 = tank1;
  this._tank2 = tank2;
  this._topLeft = [10, 30];
  this._font = 'bold 32px sans serif';
}

ScoreBoard.prototype.draw = function () {
  var line1 = 'Red: ' + this._tank1.armor();
  var line2 = 'Blue: ' + this._tank2.armor();
  ctx.font = this._font;
  ctx.fillStyle = 'black';
  ctx.fillText(line1, this._topLeft.x(), this._topLeft.y());
  ctx.fillText(line2, this._topLeft.x(), this._topLeft.y() + 30);
}

ScoreBoard.prototype.update = function () {
}

var timeLastFrame = null;
function animate(timeNow) {
  requestAnimationFrame(animate);

  var dt = timeLastFrame ? (timeNow - timeLastFrame) / 1000.0 : 0; // seconds
  timeLastFrame = timeNow;

  // clear the screen
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  drawFrame();
  update(dt);
  handleCollisions();
}

function drawFrame() {
  scene.forEach(function (o) {
    o.draw();
  });
}


function update(dt) {
  scene.forEach(function (o) {
    o.update(dt);
  });
}

function handleCollisions() {
  scene.filter(e => e.geometry).allPairs().forEach(function (pair) {
    if (pair[0].geometry().intersects(pair[1].geometry())) {
      pair[0].onCollide(pair[1]);
      pair[1].onCollide(pair[0]);
    }
  });
}

var canvas = document.querySelector('#canvas');
var ctx = canvas.getContext('2d');

var sceneRect = new Rect([0, 0], canvas.width, canvas.height);
var gravity = [0, 1000];
var groundRect = new Rect([0, canvas.height - 20], canvas.width, 20);
var ground = new Wall(groundRect, 'green');
var wallRect = groundRect.rectAboveCentered(20, 330);
var wall = new Wall(wallRect, 'black');
var tankWidth = 60;
var tankHeight = 30;
var rectTankCenter = groundRect.rectAboveCentered(tankWidth, tankHeight);
var tank1 = new Tank(rectTankCenter.translate([-350, 0]), 'red', 1 / 4 * Math.PI);
var tank2 = new Tank(rectTankCenter.translate([350, 0]), 'blue', 3 / 4 * Math.PI);
var tank1Controller = new TankController(tank1);
var tank2Controller = new TankController(tank2);
var scoreBoard = new ScoreBoard(tank1, tank2);

var scene = [
  ground,
  wall,
  tank1,
  tank2,
  scoreBoard
];

function onKeyDown(event) {
  var c1 = tank1Controller;
  var c2 = tank2Controller;

  if (event.code === 'KeyA') {
    c1.onLeftDown();
  } else if (event.code === 'KeyD') {
    c1.onRightDown();
  } else if (event.code === 'ArrowLeft') {
    c2.onLeftDown();
  } else if (event.code === 'ArrowRight') {
    c2.onRightDown();
  } else if (event.code === 'KeyW') {
    c1.onUpDown();
  } else if (event.code === 'KeyS') {
    c1.onDownDown();
  } else if (event.code === 'ArrowUp') {
    c2.onDownDown();
  } else if (event.code === 'ArrowDown') {
    c2.onUpDown();
  }
}

function onKeyUp(event) {
  var c1 = tank1Controller;
  var c2 = tank2Controller;

  if (event.code === 'KeyA') {
    c1.onLeftUp();
  } else if (event.code === 'KeyD') {
    c1.onRightUp();
  } else if (event.code === 'ArrowLeft') {
    c2.onLeftUp();
  } else if (event.code === 'ArrowRight') {
    c2.onRightUp();
  } else if (event.code === 'Space') {
    tank1.fireMissile();
  } else if (event.code === 'ShiftRight') {
    tank2.fireMissile();
  } else if (event.code === 'KeyW') {
    c1.onUpUp();
  } else if (event.code === 'KeyS') {
    c1.onDownUp();
  } else if (event.code === 'ArrowUp') {
    c2.onDownUp();
  } else if (event.code === 'ArrowDown') {
    c2.onUpUp();
  }
}

document.addEventListener('keydown', onKeyDown);
document.addEventListener('keyup', onKeyUp);

requestAnimationFrame(animate);
